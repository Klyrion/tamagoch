package tamagoch;

public class Pets {
    private int health;

    Pets() {
        health = 50;
    }

    public void sleep() {
        if (health > 0 && health < 100) {
            System.out.println("Sleeping, Health+10");
        } else {
            System.out.println("Sleeping, Health MAX!");
        }
    }

    public void eat() {
        if (health > 0 && health < 100 && health!=100) {
            System.out.println("Eating, Health+10");
        } else {
            System.out.println("Eating, Health MAX!");
        }
    }
    /**
     * Cp. Obvious: getter!
     *
     * @return {@link #health}
     */
    public int getHealth() {
        return health;
    }

    /**
     * Cp. Obvious: setter
     *
     * @param   value   {@code int} value to assign to {@link #health}
     */
    public void setHealth(int value) {
        health = value;
    }
}















