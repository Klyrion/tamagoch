package tamagoch;

import java.util.Scanner;

public class Tam {
    private static Boolean runMainMenu = true;

    public static void main(String[] args) {
        while (runMainMenu) {
            showMainMenu();
        }
    }

    private static void showMainMenu() {
        System.out.println("Type a number to select one of the following: \n" + // \n - с новой сроки
                "[1]  - КотыК.");
        System.out.println("[2] - Песык.");
        System.out.println("[3] - ПопуГейчик.");
        System.out.println("[0] - выход.");
        System.out.println("Введите номер выбранного варианта и нажмите Enter...");
        Scanner scanChoice = new Scanner(System.in);        //System.in - принимает значения введенные с клавиатуры
        int selectedProgram = scanChoice.nextInt();

        switch (selectedProgram) {
            case 1:
                catMenu();
                break;
            case 0:
            default:
                System.out.println("всЁ!");
                runMainMenu = false;
                break;
        }

    }

    private static void catMenu() {
        Cat cat = new Cat();
        cat.eat();
    }
}